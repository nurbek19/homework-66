import React, {Component, Fragment} from 'react';
import axios from 'axios';
import Backdrop from '../../components/UI/Backdrop/Backdrop';
import Spinner from '../../components/UI/Spinner/Spinner';

const withErrorHandler = (WrappedComponent) => {
    return class extends Component {
        constructor(props) {
            super(props);

            this.state = {
                loading: false
            };

            this.req = axios.interceptors.request.use(request => {
                this.setState({loading: true});
                return request;
            });

            this.res = axios.interceptors.response.use(response => {
                this.setState({loading: false});
                return response;
            });
        }

        componentWillUnmount() {
            axios.interceptors.request.eject(this.req);
            axios.interceptors.response.eject(this.res);
        }

        render() {
            return (
                <Fragment>
                    <Backdrop show={this.state.loading}>
                        <Spinner/>
                    </Backdrop>
                    <WrappedComponent {...this.props}/>
                </Fragment>
            )
        }
    }
};

export default withErrorHandler;