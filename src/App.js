import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import './App.css';
import HomePage from "./containers/HomePage/HomePage";

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" component={HomePage} />
      </Switch>
    );
  }
}

export default App;
