import React from 'react';
import Quote from './Quote/Quote';
import './QuotesList.css';

const QuotesList = props => {

    const quotes = props.quotes.map(quote => {
        return <Quote
            author={quote.author}
            body={quote.body}
            key={quote.id}
            id={quote.id}
            remove={() => props.remove(quote.id)}
        />
    });

    return (
        <div className="quotes-list">
            <h2>{props.title}</h2>
            {quotes}
        </div>
    );
};

export default QuotesList;