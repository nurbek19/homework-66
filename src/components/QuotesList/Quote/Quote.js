import React from 'react';
import './Quote.css';

const Quote = props => {
    return(
        <div className="quote">
            <div className="actions">
                <button onClick={props.remove}>Delete</button>
            </div>
            <h3>{props.body}</h3>
            <p>{props.author}</p>
        </div>
    )
};

export default Quote;