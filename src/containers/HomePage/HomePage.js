import React, {Component} from 'react';
import QuotesList from '../../components/QuotesList/QuotesList';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from 'axios';
import './HomePage.css';

class HomePage extends Component {
    state = {
        quotes: []
    };

    componentDidMount() {
        axios.get('quotes.json').then(response => {

            const quotes = [];

            for (let key in response.data) {
                quotes.unshift({...response.data[key], id: key});
            }
            this.setState({quotes});
        });
    }

    removeQuote = id => {
        axios.delete(`quotes/${id}.json`).then(() => {
            this.setState(prevState => {
                const quotes = [...prevState.quotes];
                const index = quotes.findIndex(quote => quote.id === id);
                quotes.splice(index, 1);
                return {quotes};
            })
        });
    };

    render() {
        let quotes = <QuotesList
            title="ALL"
            quotes={this.state.quotes}
            remove={this.removeQuote}
        />;

        return (
            <div className="home-page">
                {quotes}
            </div>
        );
    }
}

export default withErrorHandler(HomePage, axios);